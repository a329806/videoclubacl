const express = require('express');
const router = express.Router();
const controler = require('../controlers/copies');

/* GET users listing. */
router.get('/', controler.list);

router.get('/:id', controler.index);

router.post('/', controler.create);

router.put('/:id', controler.replace);

router.patch('/:id', controler.edit);

router.delete('/:id', controler.destroy);

module.exports = router;
