const express = require('express');
const router = express.Router();
const controler = require('../controlers/index');

/* GET home page. */
router.get('/', controler.home);

router.post('/login', controler.login);

module.exports = router;
