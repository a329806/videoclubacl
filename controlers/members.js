const express = require('express');
const Member = require('../models/member');

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Member.find().then(objs => res.status(200).json({
    message: "Lista de miembros del sistema",
    obj: objs
  })).catch(ex => res.status(500).json({
    message: "No se pudo consultar la informacion de los miembros",
    obj: ex
  }));
}

function index(req, res, next) {
  const id = req.params.id;
  Member.findOne({"_id":id}).then(obj => res.status(200).json({
    message: `Miembro almacenando con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo consultar la informacion del miembro con ID: ${id}`,
    obj: ex
  }));
}

function create(req, res, next){
  const name = req.body.name;
  const lastName = req.body.lastName;
  const phone = req.body.phone;
  const status = req.body.status;
  const city = req.body.city;
  const country = req.body.country;
  const number = req.body.number;
  const state = req.body.state;
  const street = req.body.street;

  const address = new Object({
    _city: city,
    _country: country,
    _number: number,
    _state: state,
    _street: street
  });

  let member = new Member({
    name: name,
    lastName: lastName,
    phone: phone,
    status: status,
    address: address
  });

  member.save().then(obj => res.status(200).json({
    message: "Miembro creado correctamente.",
    obj: obj
  })).catch(ex => res.status(500).json({
    message: "No se pudo almacenar el miembro.",
    obj: ex
  }));

}

function replace(req, res, next) {
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const lastName = req.body.lastName ? req.body.lastName : "";
  const phone = req.body.phone ? req.body.phone : "";
  const status = req.body.status ? req.body.status : object.status;
  const city = req.body.city ? req.body.city : "";
  const country = req.body.country ? req.body.country : "";
  const number = req.body.number ? req.body.number : "";
  const state = req.body.state ? req.body.state : "";
  const street = req.body.street ? req.body.street : "";

  const address = new Object({
    _city: city,
    _country: country,
    _number: number,
    _state: state,
    _street: street
  });

  let member = new Object({
    _name: name,
    _lastName: lastName,
    _phone: phone,
    _status: status,
    _address: address
  });

  Member.findOneAndUpdate({"_id":id}, member).then(obj => res.status(200).json({
    message: `Miembro reemplazado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo reemplazar la informacion del miembro con ID: ${id}`,
    obj: ex
  }));
}

async function edit(req, res, next) {
  const id = req.params.id;
  const name = req.body.name;
  const lastName = req.body.lastName;
  const phone = req.body.phone;
  const status = req.body.status;
  const city = req.body.city;
  const country = req.body.country;
  const number = req.body.number;
  const state = req.body.state;
  const street = req.body.street;

  const member = await Member.findOne({"_id":id});

  if(name) {
    member._name = name;
  }
  if(lastName) {
    member._lastName = lastName;
  }
  if(phone) {
    member._phone = phone;
  }
  if(status) {
    member._status = status;
  }
  if(city) {
    member._address.set("_city", city);
  }
  if(country) {
    member._address.set("_country", country);
  }
  if(number) {
    member._address.set("_number", number);
  }
  if(state) {
    member._address.set("_state", state);
  }
  if(street) {
    member._address.set("_street", street);
  }

  member.save().then(obj => res.status(200).json({
    message: `Miembro editado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo editar la informacion del miembro con ID: ${id}`,
    obj: ex
  }));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Member.remove({"_id":id}).then(obj => res.status(200).json({
    message: `Miembro eliminado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo eliminar el miembro con ID: ${id}`,
    obj: ex
  }));
}

module.exports = {
  list, index, create, replace, edit, destroy
}
