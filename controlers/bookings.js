const express = require('express');
const Booking = require('../models/booking');

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Booking.find().populate("_copy").populate("_member").then(objs => res.status(200).json({
    message: "Lista de bookings del sistema",
    obj: objs
  })).catch(ex => res.status(500).json({
    message: "No se pudo consultar la informacion de los bookings",
    obj: ex
  }));
}

function index(req, res, next) {
  const id = req.params.id;
  Booking.findOne({"_id":id}).populate("_copy").populate("_member").then(obj => res.status(200).json({
    message: `Booking almacenando con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo consultar la informacion del booking con ID: ${id}`,
    obj: ex
  }));
}

function create(req, res, next){
  const {
    copy,
    member,
    date
  } = req.body;

  let booking = new Booking({
    copy: copy,
    member: member,
    date: date
  });

  booking.save().then(obj => res.status(200).json({
    message: "Booking creado correctamente.",
    obj: obj
  })).catch(ex => res.status(500).json({
    message: "No se pudo almacenar el booking.",
    obj: ex
  }));

}

function replace(req, res, next) {
  const id = req.params.id;
  const copy = req.body.copy ? req.body.copy : object.copy;
  const member = req.body.member ? req.body.member : object.member;
  const date = req.body.date ? req.body.date : object.date;

  let booking = new Object({
    _copy: copy,
    _member: member,
    _date: date
  });

  Booking.findOneAndUpdate({"_id":id}, booking).then(obj => res.status(200).json({
    message: `Booking reemplazado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo reemplazar la informacion del booking con ID: ${id}`,
    obj: ex
  }));
}

async function edit(req, res, next) {
  const id = req.params.id;
  const {
    copy,
    member,
    date
  } = req.body;

  const booking = await Booking.findOne({"_id":id});

  if(copy) {
    booking._copy = copy;
  }
  if(member) {
    booking._member = member;
  }
  if(date) {
    booking._date = date;
  }

  booking.save().then(obj => res.status(200).json({
    message: `Booking editado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo editar la informacion del booking con ID: ${id}`,
    obj: ex
  }));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Booking.remove({"_id":id}).then(obj => res.status(200).json({
    message: `Booking eliminado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo eliminar el booking con ID: ${id}`,
    obj: ex
  }));
}

module.exports = {
  list, index, create, replace, edit, destroy
}
