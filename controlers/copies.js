const express = require('express');
const Copy = require('../models/copy');

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Copy.find().populate("_movie").then(objs => res.status(200).json({
    message: "Lista de copias del sistema",
    obj: objs
  })).catch(ex => res.status(500).json({
    message: "No se pudo consultar la informacion de las copias",
    obj: ex
  }));
}

function index(req, res, next) {
  const id = req.params.id;
  Copy.findOne({"_id":id}).populate("_movie").then(obj => res.status(200).json({
    message: `Copia almacenanda con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo consultar la informacion de la copia con ID: ${id}`,
    obj: ex
  }));
}

function create(req, res, next){
  const {
    format,
    movie,
    number,
    status
  } = req.body;

  let copy = new Copy({
    format: format,
    movie: movie,
    number: number,
    status: status
  });

  copy.save().then(obj => res.status(200).json({
    message: "Copia creada correctamente.",
    obj: obj
  })).catch(ex => res.status(500).json({
    message: "No se pudo almacenar la copia.",
    obj: ex
  }));

}

function replace(req, res, next) {
  const id = req.params.id;
  const format = req.body.format ? req.body.format : "";
  const movie = req.body.movie ? req.body.movie : object.movie;
  const number = req.body.number ? req.body.number : 0;
  const status = req.body.status ? req.body.status : object.status;

  let copy = new Object({
    _format: format,
    _movie: movie,
    _number: number,
    _status: status
  });

  Copy.findOneAndUpdate({"_id":id}, copy).then(obj => res.status(200).json({
    message: `Copia reemplazada con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo reemplazar la informacion de la copia con ID: ${id}`,
    obj: ex
  }));
}

async function edit(req, res, next) {
  const id = req.params.id;
  const {
    format,
    movie,
    number,
    status
  } = req.body;

  const copy = await Copy.findOne({"_id":id});

  if(format) {
    copy._format = format;
  }
  if(movie) {
    copy._movie = movie;
  }
  if(number) {
    copy._number = number;
  }
  if(status) {
    copy._status = status;
  }

  copy.save().then(obj => res.status(200).json({
    message: `Copia editada con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo editar la informacion de la copia con ID: ${id}`,
    obj: ex
  }));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Copy.remove({"_id":id}).then(obj => res.status(200).json({
    message: `Copia eliminada con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo eliminar la copia con ID: ${id}`,
    obj: ex
  }));
}

module.exports = {
  list, index, create, replace, edit, destroy
}
