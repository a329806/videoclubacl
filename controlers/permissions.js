const express = require('express');
const Permission = require('../models/permission');
const acl = require('acl');

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Permission.find().then(objs => res.status(200).json({
    message: "Lista de permisos del sistema",
    obj: objs
  })).catch(ex => res.status(500).json({
    message: "No se pudo consultar la informacion de los permisos",
    obj: ex
  }));
}

function index(req, res, next) {
  const id = req.params.id;
  Permission.findOne({"_id":id}).then(obj => res.status(200).json({
    message: `Permiso almacenando con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo consultar la informacion del permiso con ID: ${id}`,
    obj: ex
  }));
}

function create(req, res, next){
  const {
    description,
    resources,
    type
  } = req.body;

  let permission = new Permission({
    description: description,
    resources: resources,
    type: type
  });

  acl.allow(description, resources, type);

  permission.save().then(obj => res.status(200).json({
    message: "Permiso creado correctamente.",
    obj: obj
  })).catch(ex => res.status(500).json({
    message: "No se pudo almacenar el permiso.",
    obj: ex
  }));

}

function replace(req, res, next) {
  const id = req.params.id;
  const description = req.body.description ? req.body.description : "";
  const resources = req.body.resources ? req.body.resources : "";
  const type = req.body.type ? req.body.type : "";

  let permission = new Object({
    _description: description,
    _resources: resources,
    _type: type
  });

  acl.allow(description, resources, type);

  Permission.findOneAndUpdate({"_id":id}, permission).then(obj => res.status(200).json({
    message: `Permiso reemplazado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo reemplazar la informacion del permiso con ID: ${id}`,
    obj: ex
  }));
}

async function edit(req, res, next) {
  const id = req.params.id;
  const {
    description,
    resources,
    type
  } = req.body;

  const permission = await Permission.findOne({"_id":id});

  if(description) {
    permission._description = description;
  }
  if(resources) {
    permission._resources = resources;
  }
  if(type) {
    permission._type = type;
  }

  acl.allow(description, resources, type);

  permission.save().then(obj => res.status(200).json({
    message: `Permiso editado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo editar la informacion del permiso con ID: ${id}`,
    obj: ex
  }));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Permission.remove({"_id":id}).then(obj => res.status(200).json({
    message: `Permiso eliminado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo eliminar el permiso con ID: ${id}`,
    obj: ex
  }));
}

module.exports = {
  list, index, create, replace, edit, destroy
}
