const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const acl = require('acl');

const User = require('../models/user');

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  let page = req.params.page ? req.params.page : 1;
  User.paginate({}, {page:page, limit:3}).then(objs => res.status(200).json({
    message: "Lista de usuarios del sistema",
    obj: objs
  })).catch(ex => res.status(500).json({
    message: "No se pudo consultar la informacion de los actores",
    obj: ex
  }));
}

function index(req, res, next) {
  const id = req.params.id;
  User.findOne({"_id":id}).then(obj => res.status(200).json({
    message: `Ususario almacenando con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo consultar la informacion del usuario con ID: ${id}`,
    obj: ex
  }));
}

async function create(req, res, next){
  const name = req.body.name;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const permissions = req.body.permissions;

  async.parallel({
    salt:(callback) => {
      bcrypt.genSalt(10, callback);
    }
  }, (err, result) => {
    bcrypt.hash(password, result.salt, async (err, hash) =>{

      let user = new User({
        name: name,
        lastName: lastName,
        email: email,
        password: hash,
        salt: result.salt,
        permissions: permissions
      });


      await user.save().then(obj => {
        res.status(200).json({
          message: "Usuario creado correctamente.",
          obj: obj
        });
      }).catch(ex => res.status(500).json({
        message: "No se pudo almacenar el usuario.",
        obj: ex
      }));

    });

  });
}

function replace(req, res, next) {
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const lastName = req.body.lastName ? req.body.lastName : "";
  const email = req.body.email ? req.body.email : "";
  const password = req.body.password ? req.body.password : "";
  const permissions = req.body.permissions ? req.body.permissions : object.permissions;

  let user = new Object({
    _name: name,
    _lastName: lastName,
    _email: email,
    _password: password,
    _permissions: permissions
  });

  // acl.addUserRoles(id, permissions);

  User.findOneAndUpdate({"_id":id}, user).then(obj => res.status(200).json({
    message: `Usuario reemplazado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo reemplazar la informacion del usuario con ID: ${id}`,
    obj: ex
  }));
}

function edit(req, res, next) {
  const id = req.params.id;
  const name = req.body.name;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const permissions = req.body.permissions;

  let user = new Object();

  if(name) {
    user._name = name;
  }
  if(lastName) {
    user._lastName = lastName;
  }
  if(email) {
    user._email = email;
  }
  if(password) {
    user._password = password;
  }
  if(permissions) {
    user._permissions = permissions;
    // acl.addUserRoles(id, permissions);
  }
  User.findOneAndUpdate({"_id":id}, user).then(obj => res.status(200).json({
    message: `Usuario editado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo editar la informacion del usuario con ID: ${id}`,
    obj: ex
  }));
}

function destroy(req, res, next) {
  const id = req.params.id;
  User.remove({"_id":id}).then(obj => res.status(200).json({
    message: `Usuario eliminado con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo eliminar el usuario con ID: ${id}`,
    obj: ex
  }));
}

module.exports = {
  list, index, create, replace, edit, destroy
}
