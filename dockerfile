FROM node
MAINTAINER Ruben Dominguez
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start
